Deep Learning Challenge

Participants :
- ANDRIAMASY Yandi
- DE MONTBRUN Benoît
- LE GALL Corentin
- MESSAOUDI Rayan

How to use our script :
- the call python3 main.py train PATH*TO*TRAINING*SET is training the best model we deliver with the training set given in the path PATH*TO*TRAINING*SET.
- the call python3 main.py test PATH*TO*CHECKPOINT PATH*TO*TEST*SET PATH*TO*TRAINING*SET is loading the model whose checkpoint full path is PATH*TO*CHECKPOINT, testing the model on the data provided in PATH*TO*TEST*SET and outputting the label CSV file ready for submission